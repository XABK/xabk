# Overview
Это обучающий сайт для тестирования разработчиков в веб-студии "Цифровой век".
Ваша задача - форкнуть реп, выполнить задание и отправить пулл-реквест.
Задание на английском, потому что нормальный разработчик должен уметь пользоваться Яндекс.Переводчиком.
Обычно время выполнения этого задания не превышает 2 часа. Было бы замечательно, если бы вы засекли ваше время выполнения и сообщили его в комментарии к пулл-реквесту.

## Question 1

### The Problem:

Your goal is to implement an interactive navigation bar that slides down and provides the user with sub-navigation options when the user clicks on a link in the main nav. You have already written the HTML and CSS for the navigation bar and the last step is to add functionality with JavaScript.

### What to Code:

Add necessary code to nav.js

Write JavaScript to make the menu interactive.

When the user clicks on a navigation item, you will need to:

- Add a class to the link the user clicked on (hint: you can use the “active” class)
- Slide down the secondary nav bar
- Fade in the related sub-navigation section.

If the user clicks on a link that is already active you will need to:
- Remove the active state from all menu items
- Slide up the secondary nav bar.

You will not need to alter any HTML or CSS.

The behavior you are trying to create:

![Layout](assets/nav.gif)

## Question 2

### The Problem:

The Designer you are working with has designed a slideshow to add visual interest to the page. In order to make the slideshow more extensible, you decide to add it to the page via Javascript.

### What to Code:

Add necessary code to slideshow.js

Provided for you is a variable "slideshow" that is composed of slideshow entries, each containing an "image" and "caption". Parse and create a photo slideshow with an `<img>` for each slideshow entry using the "image" value for the "src" attribute and the "caption" value for the "title" and "alt" attributes. Populate the ".gallery-wrapper" div with these images.

Hint: Images can follow this form:

```html
<img src="images/gallery_1.jpeg" alt="Moon above mountains" title="Cloudy with a chance of moon" class="current">
<img src="images/gallery_2.jpeg" alt="Half moon mountain" title="Half moon mountain">
<img src="images/gallery_3.jpeg" alt="Moonrise" title="Moonrise">
```

Apply the class ".current" to the first image to make it visible (CSS already exists).

Set the "#caption" text to be the title attribute of the first image. The "#caption" should always be the title attribute of the ".current" image.

The left button should be disabled when the first image is show, and the right button should only be disabled when the last image is shown.

When a user clicks on the right button:
- If the ".current" image is the last image:
  * Nothing should happen
- Otherwise:
  * Remove the class ".current" from the current image and apply it to the next image.
  * Update "#caption" text with title attribute from new current image.
  * If you've reached the first or last slide, disable the appropriate arrow.

When a user clicks on the left button:
- If the ".current" image is the first image:
  * Nothing should happen
- Otherwise:
  * Remove the class ".current" from the current image and apply it to the previous image.
  * Update "#caption" text with title attribute from new current image.
  * If you've reached the first or last slide, disable the appropriate arrow.

You will not need to alter any HTML or CSS.

The behavior you are trying to create:

![Layout](assets/slideshow.gif)

## Question 3

### The Problem:

In order to improve our load time and not use valuble real estate on the page, we are going to load posts from the site archive asynchronously. When the page first loads, the first 4 posts are hard-coded, then users will be given the option to "Load More".

### What to Code:

Add necessary code to archive.js

When a user clicks on the "Load More" button:
- Disable the button and have it display: "Exploring the archive ..." along with an animated loading icon (use "fa fa-circle-o-notch fa-spin" from the Font Awesome library)
- Perform an AJAX request that returns the next 4 most recent articles from the archive
- Attach a callback function:
  * If there are posts returned:
    * Add additional article's as needed to the DOM, following the conventions of the existing markup
    * Enable the button and have it display "Load More"
  * If there are no more posts:
    * Disable the Load More button and have it display "End of Archive"
  * If the request fails or there is an error, disable the button and have it display "Something Went Wrong" along with an error icon (use "fa fa-exclamation-triangle" from the Font Awesome library)

Hint: Other possible states of Load More button:

```html
<button id="load-more" disabled>Exploring the Archive <i class="fa fa-circle-o-notch fa-spin"></i></button>
<button id="load-more" disabled>Something Went Wrong <i class="fa fa-exclamation-triangle"></i></button>
<button id="load-more" disabled>End of Archive</button>
```

### How to Get Archive Posts:

Hitting the following URL:
https://credentials-api.generalassemb.ly/explorer/posts
- returns the 4 most recent posts from the archive

https://credentials-api.generalassemb.ly/explorer/posts?offset=4
- returns the 4 most recent posts from the archive, starting from the index of the given offset

Each call to the service will return up to 4 posts, each with:

- category
- title
- date
- blurb

The "category" will be the Font Awesome class needed to display the appropriate icon (in the form "fa-____"). For more information on Font Awesome, see [this documentation](https://fortawesome.github.io/Font-Awesome/)

The returned JSON object will resemble the following:

```
{
  "posts": [
    {
      "category": "fa-space-shuttle",
      "title": "Apollo 13",
      "date": "Sep. 1, 2015",
      "blurb": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam aperiam eos quia, unde corporis, quos reprehenderit doloremque blanditiis dignissimos laborum repellat aliquam vel officia possimus, debitis esse adipisci natus, molestias."
    },
    ...
  ]
}
```

Any call that specifies an offset beyond the number of items in the archive will return:

```
{
  "posts": []
}
```

You will not need to alter any HTML or CSS.

The behavior you are trying to create:

![Layout](assets/archive.gif)

### Grading:

Your code will be graded based on:

- Function: Whether it meets each functional requirement specified in this prompt
- Efficiency: its maintainability and run time
- Consistency: Whether it is written consistently with the rest of the code base. Specifically:
  * Semicolons: Always use.
  * Naming: Use camel case everywhere but constants. Use all caps snake case for constants.
  * Strings: Use single quotes.
  * Spacing: Indent new lines 4 spaces, put a space between arguments in a function, and for parentheses/curly brackets, no spaces after opening bracket and before closing